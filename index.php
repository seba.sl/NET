<?php
header('Content-Type: text/html; charset=UTF-8');

if(!isset($_SESSION)) session_start();
include("inc/cnxi.php");

?>
<html lang="en">
<head>
	<meta name="google-signin-scope" content="profile email">
	<meta name="google-signin-client_id" content="988620959924-vv3tc5t7c3urliekm8jsgkn59iubeqn9.apps.googleusercontent.com">
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<script type="text/javascript" src="inc/ajax.js?v=1"></script>
	<script type="text/javascript" src="inc/jquery.js?v=1"></script>
	<script type="text/javascript" src="inc/jquery-ui.min.js?v=1"></script>
	<script type="text/javascript" src="inc/bailac.js?v=1"></script>
	<link type="text/css" rel="stylesheet" href="inc/style.css?v=1" />
</head>
<body><div id='layers'>
<div id='layer0' class='layer'>
	<header></header>
	<div class="block" style="width:300px;margin:20px auto;">
		<h1>Bienvenido a BailacNET</h1>
		<div style="margin:10px 0;">Inicie sesión con una cuenta @bailacthor.com para continuar</div>
		<div id="btn_g" class="g-signin2" data-onsuccess="onSignIn" data-theme="dark" style="float: right;"></div>
		<div style="clear: both;"></div>
		<div class="warning" id="wrong_account" style="margin: 10px 0;"></div>
	</div>
</div>
</div>
</body>
</html>
<script>
var profile='';

function onSignIn(googleUser) {
	profile = googleUser.getBasicProfile();
	var id_token = googleUser.getAuthResponse().id_token;
	
	if(profile.getEmail().split(/@/).pop()!='bailacthor.com'){
		googleUser.disconnect();
		profile='undefined';
		$('#wrong_account').html("Se requiere una cuenta @bailacthor.com para continuar");
	}else{
		LINK_ajax('main.php?mail_user='+profile.getEmail()+'&img_url='+profile.getImageUrl()+'&name_user='+profile.getName()+'&x=', 'layer0');
	}
}
function signPrompt() {
	$('#accountMenu').fadeIn();
}
function signIn() {
	var auth2 = gapi.auth2.getAuthInstance();
	auth2.signIn({prompt:'select_account'});
}
function signOut() {
	var auth2 = gapi.auth2.getAuthInstance();
	auth2.signOut().then(function () {
		console.log('Session finalizada.');
		location.reload();
	});
}
if(profile == ''){
	$('#btn_g').css('display','visible');
}
</script>
