<?php
if(!function_exists(hiddens)){
	function hiddens($a){
		foreach($a as $k=>$v)echo "<input type=\"hidden\" id=\"$k\" name=\"$k\" value=\"$v\" />";
	}
}

if(!function_exists(pagination)){
	function pagination($num,$jx,$limit,$tg){
		global $lang;
		if($num>0){
			if(substr($jx,0,5)=="POST:"){
				$jx=substr($jx,5);
				$AJX='POST';
			}else $AJX='GET';
			
			$tpage=ceil($num/$limit);
			$tgs='filtr_pag'.$tg;
			if(!$_GET[$tgs])$_GET[$tgs]=1; ?>

			<div class='pag_container'>
				<div class='pag_results'><?php echo "$num Resultados"; ?></div>
				<?php $dis=($_GET[$tgs]>1? '':'disabled'); ?>
				<input class="icomoon_ultimate <?php echo $dis; ?>" type="button"  value="" 
					id="prev_pag_first<?php echo $tg ?>" name="prev_pag_first<?php echo $tg ?>"
					onclick="<?php echo ($_GET[$tgs]>1? "$('#filtr_pag$tg').val('1');{$AJX}_ajax($jx);":''); ?>" <?php echo $dis; ?> />
				
				<input class="icomoon_ultimate <?php echo $dis; ?>" type="button"  value="" 
					id="prev_pag<?php echo $tg ?>" name="prev_pag<?php echo $tg ?>"
					onclick="<?php echo ($_GET[$tgs]>1? "$('#filtr_pag$tg').val($_GET[$tgs]-1);{$AJX}_ajax($jx);":''); ?>" <?php echo $dis; ?> />
				
				<select id="filtr_pag<?php echo $tg ?>" name="filtr_pag<?php echo $tg ?>" 
					class="filtr_pag<?php echo $tg ?> filtr_pag" 
					onchange="<?php echo $AJX."_ajax($jx);"; ?>"><?php 
					for ($i=1; $i <= $tpage; $i++) {?>
					<option value='<?php echo $i ?>'<?php if($i==$_GET[$tgs]) echo " selected='selected'"; ?>><?php echo $i ?></option><?php 
					} ?>
				</select>
				
				<?php $dis=($_GET[$tgs]<$tpage? '':'disabled'); ?>
				<input class="icomoon_ultimate <?php echo $dis; ?>" type="button" value=""  
					id="next_pag<?php echo $tg ?>" name="next_pag<?php echo $tg ?>"
					onclick="<?php echo ($_GET[$tgs]<$tpage? "$('#filtr_pag$tg').val($_GET[$tgs]+1);{$AJX}_ajax($jx);":''); ?>" <?php echo $dis; ?> />
				
				<input class="icomoon_ultimate <?php echo $dis; ?>" type="button"  value="" 
					id="next_pag_last<?php echo $tg ?>" name="next_pag_last<?php echo $tg ?>"
					onclick="<?php echo ($_GET[$tgs]<$tpage? "$('#filtr_pag$tg').val('$tpage');{$AJX}_ajax($jx);":''); ?>" <?php echo $dis; ?> />
			</div> <?php
   		}
	}
}

if(!function_exists('edi_table')){
	function edi_table($pref,$suff,$fiel,$iden,$type){
		if(is_array($type)){ // select
			$return="<select data-pref='$pref' data-suff='$suff' data-fiel='$fiel' data-iden='$iden' data-rand='".md5($fiel.'net'.$iden)."'>";
			foreach($type as $k=>$v){
				$return.="<option value='$k'>$v</option>";
			}
			$return.="</select>";
			return $return;
		}else{
			return "<input type='$type'  data-pref='$pref' data-suff='$suff' data-fiel='$fiel' data-iden='$iden' data-rand='".md5($fiel.'net'.$iden)."' >";
		}
	}
}
?>