// JavaScript Document

var layer=0;
var sourc="nonc/index";
var sources = new Array("nonc/index");
var editingform=0;

var focused='undefined'; // elemento en foco

function layer_open(sour,getajax){
	layer++;
	sourc=sour;
	sources[layer]=sour;
	if(typeof getajax==="undefined")
		$('#layers').append("<div id='layer" + layer + "' class='layer" + layer + " layer'><script language='javascript'>LINK_ajax('" + sourc + "', 'layer" + layer + "');</script></div>");
	else
		$('#layers').append("<div id='layer" + layer + "' class='layer" + layer + " layer'><script language='javascript'>GET_ajax('" + sourc + "', 'layer" + layer + "','" + getajax + "');</script></div>");
	$("body").animate({scrollTop:0}, '500');
}
function layer_close(){
	var procced=0;
	if(editingform==1){
		r_u_sure('The form has changed, discard changes?','editingform=0;layer_close()');
	}else{
		procced=1;
	}
	if(procced){
		$('#layer'+layer).remove();
		//$.ajax({
		//	url: 'inc/drop.php?lay=' + layer,
		//	cache: false
		//});
		layer--;
		sourc=sources[layer];
		LINK_ajax(sourc,'layer' + layer);
		$('#div_contents_disabled').attr('id','div_contents');
		$('#form_main_disabled').attr('id','form_main');
	}
}
function layer_close_all(){
	while(layer>0){
		$('#layer'+layer).remove();
		
		layer--;
	}
	sourc=sources[layer];
}

function textarea_resize(that){
	if(that.val().length>60){
		that.css({'font-size':'16px'});
	}else{
		that.css({'font-size':'24px'});
	}
	if(that[0].scrollHeight==that.height()) that.height(10);
	that.height(that[0].scrollHeight);
}
function textarea(){
	$("textarea").off("scroll");
	$("textarea").off("keyup");
	$("textarea").scroll(function(e){
		if(this.scrollHeight==$(this).height()) $(this).height($(this).height()-1);
		$(this).height(this.scrollHeight);
	});
	$("textarea").keyup(function(e){
		textarea_resize($(this));
	});
	$("textarea").each(function(e){
		textarea_resize($(this));
	});
}

function edi_table(tabl_id,selectable){ // UPDATE tab_pre__tab_suf SET field=values WHERE tab_pre__tab_suf.iden=iden
	if(selectable==="undefined")selectable=0;
	
	$('#'+tabl_id+' .edi_table div:last-child').hide();
	$('#'+tabl_id+' .edi_table div:first-child').height(23);
	
	$('#'+tabl_id+' .edi_table div:first-child').click(function (){
		$('#'+tabl_id+' .edi_table div:first-child').show();
		$('#'+tabl_id+' .edi_table div:last-child').hide();
		
		$(this).hide();
		$(this).next().show();
		$(this).next().find('input:not([type="date"])').val($(this).text()).focus();
		$(this).next().find('input[type="date"]').val('2000-' + $(this).text().substr(3) + '-' + $(this).text().substr(0,2)).focus();
		
		$(this).next().find('select').find("option:contains(" + $(this).text() + ")").attr("selected", true);
		$(this).next().find('select').focus().trigger('open');
	});
	if(!selectable){
		$('#'+tabl_id+' .edi_table div:last-child input, #'+tabl_id+' .edi_table div:last-child select').blur(function (){
			$(this).parent().hide();
			$(this).parent().prev().show();
		});
	}
	$('#'+tabl_id+' .edi_table div:last-child input, #'+tabl_id+' .edi_table div:last-child select').change(function (){
		var t=$(this);
		var tabl=t.data('pref') + '__' + t.data('suff');
		$.ajax({
			method: "GET",
			url: 'inc/query.php',
			data: {
				pref: t.data('pref'),
				suff: t.data('suff'),
				fiel: t.data('fiel'),
				iden: t.data('iden'),
				rand: t.data('rand'),
				type: $(this).attr('type'),
				valu: $(this).val()
			},
			that: $(this)
		}).done(function(d){
			if(this.that.is("select")){
				console.log(d);
				this.that.parent().prev().text(		this.that.find("option:selected").text()		);
				this.that.blur();
			}else{
				this.that.parent().prev().text(d);
			}
		});
	});
}
