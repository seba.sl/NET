// JavaScript Document
//functions
function prepareCode(){
	
}
function datepickers(){
	//datetime pickers
	$('.dateonly').each(function() {
		var ran_sid=$(this).attr('id');
		var ran_max=$('#' + ran_sid).data('max');
		var ran_min=$('#' + ran_sid).data('min');
		$(this).datepicker({
			onSelect :function(ct){
				if(ran_max){
					$( "#" + ran_max).datepicker( "option", "maxDate", ct );
				}
				if(ran_min){
					$( "#" + ran_min).datepicker( "option", "minDate", ct );
				}
				$('#'+ran_sid).trigger('change');
			}
		});
	});
	
	$('.datetime').each(function() {
		var ran_sid=$(this).attr('id');
		var ran_max=$('#' + ran_sid).data('max');
		var ran_min=$('#' + ran_sid).data('min');
		$(this).datetimepicker({
			step:15,
			format:'Y-m-d H:i',
			onShow:function(ct){
				if(ran_max){
					ran_val=jQuery('#' + ran_max).val();
					ran_val=ran_val.substr(0,10);
					this.setOptions({
						maxDate:ran_val?ran_val:false
					});
				}
				if(ran_min){
					ran_val=jQuery('#' + ran_min).val();
					ran_val=ran_val.substr(0,10);
					this.setOptions({
						minDate:ran_val?ran_val:false
					});
				}
			}
		});
	});
}
function prepareContent(){
	//datetime pickers
	//datepickers();
	
	// Disabling forms for read only perms
	//$('.formsOff input,.formsOff select').prop("disabled",true);
	//$('.formsOff input,.formsOff select').removeAttr("name",""); // seriously!
	
	//var l="#layer" + layer + " ";
	//var v=l + "input[type='text']:not(.nofocus)," + l + "input[type='button']:not(.nofocus)," + l + "select:not(.nofocus)";
	//
	//if(focused!='undefined' && $(l + focused).length){
	//	$(l + focused).focus();
	//}else if($(v).first().length && $(v).first().attr('id')!='undefined'){
	//	$(v).first().focus();
	//	focused='#' + $(v).first().attr('id');
	//}else{
	//	focused='undefined';
	//}
	//
	//$('input,select').focus(function (){
	//	focused='#' + $(document.activeElement).attr('id');
	//});
}
//ajax
function LINK_ajax(http, div_name) {
	/*var LINK_xmlhttp = false;
	try { LINK_xmlhttp = new ActiveXObject("Msxml2.XMLHTTP"); }
	catch (e) { try { LINK_xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); }
	catch (e) { try { LINK_xmlhttp = new XMLHttpRequest(); }
	catch (e) { LINK_xmlhttp = false; }}}
	if (!LINK_xmlhttp) return null;*/
	$('.line').addClass('loading');
	$('body').css('cursor','wait');
	$('#'+div_name).animate({opacity:0},500);
	prepareCode();
	//jQuery('#img_load').css('display','block').animate({opacity:1},500);				LOADER
	/*LINK_xmlhttp.open("GET", http+'.php', true);
	
	LINK_xmlhttp.onreadystatechange = function() {
		if (LINK_xmlhttp.readyState == 4) {
			document.getElementById(div_name).innerHTML = LINK_xmlhttp.responseText + ' ';
			jQuery('#'+div_name).stop().css('opacity','0');
			jQuery('body').css('cursor','default');
			jQuery('#'+div_name).animate({opacity:1},500);
			jQuery('#img_load').stop().animate({opacity:0},500).css('display','none');
			upd_fleet();
		}
	}
	LINK_xmlhttp.send(null);*/
	///////////// 
	if(div_name.substring(0,5)=='layer'){
		
		/*var rep_http = http.replace(/&/g,"||");
		
		if(rep_http.indexOf('.')>0 && 0){
			pos=rep_http.indexOf('.');
			sourc=rep_http.substring(0,pos);
		}else{
			pos='-';
			sourc=rep_http;
		}*/
		
		sources[layer]=sourc;
		
		//alert(sourc + "_____" + pos + "____" + rep_http);
		
		// $.ajax({
		// 	url: 'inc/save.php?lay=' + div_name + '&htt=' + rep_http,
		// 	cache: false
		// });
	}
	if(http=="widgets"){
		$('body').css('overflow','hidden');
	}else{
		$('body').css('overflow','auto');
	}
	
	$.ajax({
		url: http+'.php',
		cache: false
	}).done(function( html ) {
		$('#'+div_name).html(html);

		if(div_name=='modal') $("input[name='modal_cancel']").focus();
		else prepareContent();
			
		document.getSelection().removeAllRanges();
		$('#'+div_name).stop().css('opacity','0');
		$('body').css('cursor','default');
		$('.line').removeClass('loading');
		$('#'+div_name).animate({opacity:1},500);
		//jQuery('#img_load').stop().animate({opacity:0},500).css('display','none');
		if(div_name=='layer0') $('#headmenu').val(sourc);
		
		if(typeof delayed_action === "function"){
			delayed_action();
			delayed_action=0;
		}
	});
	/////////////
	return false;
}
function FAST_ajax(http, div_name) {
	var LINK_xmlhttp = false;
	try { LINK_xmlhttp = new ActiveXObject("Msxml2.XMLHTTP"); }
	catch (e) { try { LINK_xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); }
	catch (e) { try { LINK_xmlhttp = new XMLHttpRequest(); }
	catch (e) { LINK_xmlhttp = false; }}}
	if (!LINK_xmlhttp) return null;
	
	LINK_xmlhttp.open("GET", http+'.php', true);
	LINK_xmlhttp.onreadystatechange = function() {
		if (LINK_xmlhttp.readyState == 4) {
			document.getElementById(div_name).innerHTML = LINK_xmlhttp.responseText + ' ';
			//document.getSelection().removeAllRanges();
			//upd_fleet();
			/*try{
				if(document.getElementById('fleet_bottom').innerHTML=="&nbsp;" && document.getElementById('lightbox').style.visibility=='hidden'){
					upd_fleet();
				}
			}catch(e){}*/
		}
	};
	LINK_xmlhttp.send(null);
	return false;
}
/*function LINKGET_ajax(http, div_name) {
	var LINK_xmlhttp = false;

	try { LINK_xmlhttp = new ActiveXObject("Msxml2.XMLHTTP"); }
	catch (e) { try { LINK_xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); }
	catch (e) { try { LINK_xmlhttp = new XMLHttpRequest(); }
	catch (e) { LINK_xmlhttp = false; }}}
	if (!LINK_xmlhttp) return null;
	
	//document.getElementById('loading').style.visibility="visible";
		
	LINK_xmlhttp.open("GET", http, true);
	LINK_xmlhttp.onreadystatechange = function() {
		if (LINK_xmlhttp.readyState == 4) {
			//document.getElementById('loading').style.visibility="hidden";
			document.getElementById(div_name).innerHTML = LINK_xmlhttp.responseText + ' ';
		}
	}
	LINK_xmlhttp.send(null);
	
	if(message){
		messagebox(message);
		message=0;
	}

	return false;
}*/
function SAVE_ajax(frm_name) {
	editingform=1;
	frm = document.getElementById(frm_name);
	url = "frm_name=" + frm_name;
	for (i = 0; i < frm.elements.length; i++) { 
		//frm.elements[i].disabled = true;
		if (frm.elements[i].type == "select-multiple") {
			var sel=frm.elements[i];
			for (var si = 0, m = sel.length; si < m; si++) {
				if (sel[si].selected) {
					//query.push(sel[si].value);
					url = url + "&" + sel.name + "=" + encodeURIComponent(sel[si].value);
				}
			}
		}if (frm.elements[i].type == "checkbox") {
			if (frm.elements[i].checked)
				url = url + "&" + frm.elements[i].name + "=" +  encodeURIComponent(frm.elements[i].value);
			else
				url = url + "&" + frm.elements[i].name + "=0";
		}else if (frm.elements[i].type == "radio") {
			if (frm.elements[i].checked){
				url = url + "&" + frm.elements[i].name + "=" + encodeURIComponent(frm.elements[i].value);
			}
		}else if(frm.elements[i].name == "nosend"){
			url = url + "&" + frm.elements[i].name + "=1";
		}else{
			url = url + "&" + frm.elements[i].name + "=" + encodeURIComponent(frm.elements[i].value);
		}
	}
	
	var rep_http = url.replace(/&/g,"||");
	//alert('sourc=' + sourc);
	//alert('rep_http=' + rep_http);
	// $.ajax({
	// 	url: 'inc/save.php?lay=' + layer + '&htt=' + sourc + ".php?" + rep_http,
	// 	cache: false
	// });
}
function GET_ajax(http,div_name,frm_name,http2,div_name2,frm_name2,http3,div_name3,frm_name3){
	var GET_xmlhttp = false;
	var frm = false;
	var url = "";
	try { GET_xmlhttp = new ActiveXObject("Msxml2.XMLHTTP"); }
	catch (e) { try { GET_xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); }
	catch (e) { try { GET_xmlhttp = new XMLHttpRequest(); }
	catch (e) { GET_xmlhttp = false; }}}
	if (!GET_xmlhttp) return null;
	prepareCode();
	$('.line').addClass('loading');
	$('body').css('cursor','wait');
	//jQuery('#img_load').css('display','block').animate({opacity:1},500);
	//$('.top').css({'background-size':'20%,auto','background-image':'url(img/loading.gif),url(img/000_75.png)'});
	//frm = document.getElementById(frm_name);
	if(layer) // si estamos dentro de un layer, se concatena automaticamente
		frm = $('.layer' + layer + ' #' + frm_name)[0];
	else
		frm = document.getElementById(frm_name);
	if(frm===undefined)
		frm = document.getElementById(frm_name);
	
	url = "frm_name=" + frm_name;
	for(i=0;i<frm.elements.length;i++){
		//frm.elements[i].disabled = true;
		var fval=encodeURIComponent(frm.elements[i].value.replace(/"/g,"'"));//evita las doble comillas y convierte en url
		if (frm.elements[i].type == "checkbox") {
			if (frm.elements[i].checked){
				url = url + "&" + frm.elements[i].name + "=" +  fval;
			}else{
				url = url + "&" + frm.elements[i].name + "=0";
			}
		}else if (frm.elements[i].type == "radio") {
			if (frm.elements[i].checked){
				url = url + "&" + frm.elements[i].name + "=" + fval;
			}
		}else if (frm.elements[i].type == "button" || frm.elements[i].type == "submit") {
			//do not send
		}else if(fval==""){
			//do not send
		}else{
			url = url + "&" + frm.elements[i].name + "=" + fval;
		}
	}
	
	var rep_http = url.replace(/&/g,"||");
	rep_http = rep_http.replace("bodtype","xbodtype");
	//console.log(url);
	//alert('sourc=' + sourc);
	//alert('rep_http=' + rep_http);
	// $.ajax({
	// 	url: 'inc/save.php?lay=' + layer + '&htt=' + sourc + ".php?" + rep_http + "||ext=",
	// 	cache: false
	// });
		
	GET_xmlhttp.open("GET", http + ".php?" + url, true);
	GET_xmlhttp.setRequestHeader('Content-type', "application/x-www-form-urlencoded; charset=utf-8");
	GET_xmlhttp.onreadystatechange = function() {
		if (GET_xmlhttp.readyState == 4) { 
			for (i = 0; i < frm.elements.length; i++) {
				if (frm.elements[i].type == "password"){
					frm.elements[i].value = "";
					//frm.elements[i].disabled = false;
				}
			}
			if(div_name!="nowhere" && div_name!="print"){
				if(layer && div_name.substring(0,5)!='layer') // si estamos dentro de un layer y no se define cual es, se concatena automaticamente
					$('#layer' + layer + ' #' + div_name).html(GET_xmlhttp.responseText);
				else
					document.getElementById(div_name).innerHTML = GET_xmlhttp.responseText + ' ';
				prepareContent();
			}
			
			var eval_text=GET_xmlhttp.responseText;
			var tajxdelay;
			for (tajxdelay=0;eval_text.indexOf("<script>") >= 0 && (eval_text.indexOf("</script>") > eval_text.indexOf("<script>"));tajxdelay+=0.1) {
				var x = eval_text.indexOf("<script>") + "<script>".length;
				var y = eval_text.indexOf("</script>") - x;
				setTimeout(eval_text.substr(x, y),tajxdelay);
				eval_text = eval_text.substr(x*1+y*1+1);
			}
			//jQuery('#img_load').stop().animate({opacity:0},500);
			//$('.top').css({'background-size':'auto','background-image':'url(img/000_75.png)'});
			//setTimeout("jQuery('#img_load').css('display','none')",500);
			if(typeof http2!=="undefined" && typeof div_name2!=="undefined" && typeof frm_name2!=="undefined" && typeof http3!=="undefined" && typeof div_name3!=="undefined" && typeof frm_name3!=="undefined"){
				DelayGET_ajax(http2,div_name2,frm_name2,http3,div_name3,frm_name3);
			}else if(typeof http2!=="undefined" && typeof div_name2!=="undefined" && typeof frm_name2!=="undefined"){
				DelayGET_ajax(http2,div_name2,frm_name2);
			}else{
				$('.line').removeClass('loading');
				jQuery('body').css('cursor','default');
			}
		}
	};
	GET_xmlhttp.send(null);
	return false;
}
function DelayGET_ajax(http,div_name,frm_name,http2,div_name2,frm_name2) {
	if(typeof http2!=="undefined" && typeof div_name2!=="undefined" && typeof frm_name2!=="undefined"){
		setTimeout("GET_ajax('"+http+"','"+div_name+"','"+frm_name+"','"+http2+"','"+div_name2+"','"+frm_name2+"')",500);
	}else{
		setTimeout("GET_ajax('"+http+"','"+div_name+"','"+frm_name+"')",500);
	}
}

function POST_ajax(http,div_name,frm_name) { console.log(http + ',' + div_name + ',' + frm_name);
	
	jQuery('body').css('cursor','wait');
	
	var formData = new FormData(document.getElementById(frm_name));
	
	//document.getElementById(div_name).innerHTML = ' ';
	
	$.ajax({
		url: http + ".php",
		type: "post",
		dataType: "html",
		data: formData,
		cache: false,
		contentType: false,
		processData: false
	})
    .done(function(res){
		if(layer && div_name.substring(0,5)!='layer') // si estamos dentro de un layer y no se define cual es, se concatena automaticamente
			$('#layer' + layer + ' #' + div_name).html(res);
		else
			document.getElementById(div_name).innerHTML = res + ' ';
		prepareContent();
		jQuery('body').css('cursor','default');
    });
	
	return false;
}



























