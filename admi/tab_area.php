<?php
if(!isset($_SESSION)) session_start();
if(!isset($cnx)) include("../inc/cnxi.php");
include('../inc/function.php');

$where="";
foreach($_GET as $k=>$v){
	$$k=$cnx->real_escape_string($v); 
	if(!in_array($k,array('frm_name','_','order_by_a','asort_by','filtr_pag','co_a','add_a','del_a'))){
		if(is_numeric($k))$where.=" AND $k='$v'";
		else $where.=" AND $k like '%$v%'";
	}
}
/* Add Area */
if($name_area){
	$cnx->query("INSERT IGNORE INTO stru__area(name_area) VALUES('$name_area')");
}
/* Del Area */
if($_GET['del_a']){
	$del=$cnx->real_escape_string($_GET['del_a']);
	// lo eliminará dejando a los usuarios con esa area en NULL y eliminando los registros relacionados de la tabla stru_pxa y stru_uxa
	$cnx->query("DELETE FROM stru_pxa WHERE iden_area=$del");
	
	$cnx->query("UPDATE stru__user SET area_user=NULL WHERE area_user=$del"); // esto hay que eliminarlo cuando desaparezcan esos campos
	//$cnx->query("DELETE FROM stru_uxa WHERE iden_area=$del"); // esto hay que descomentarlo cuando exista esta tabla
	
	$cnx->query("DELETE FROM stru__area WHERE iden_area=$del");
}

/* Order */
if($_GET[co_a]){
	if(!$asort_by || $asort_by=="ASC") $asort_by="DESC"; 
	elseif($asort_by=="DESC")$asort_by="ASC"; 
}

if(!function_exists(orderbtn)){
	function orderbtn($tg){
		$t_class=($_GET[order_by_a]==$tg)?'active':'';
		if(!isset($_GET[order_by_a]) || $_GET[order_by_a]<>$tg) $t_icon='';
		else $t_icon=(($_GET[asort_by]=='DESC' && $_GET[order_by_a]==$tg)?'':'');
		
		echo "<span class=\"icomoon_ultimate $t_class\" > $t_icon </span>";
	}
}
$order=($order_by_a and $asort_by)?"$order_by_a $asort_by":"name_area ASC";
/* Order */
$MQ=$cnx->query("SELECT stru__area.* FROM stru__area WHERE 1 $where ORDER BY $order");

/* Pagination */
$reg_num=$MQ->num_rows;
$reg_page=10;

$page=($filtr_pag*$reg_page);
if(!$page)$page=$reg_page;
if($filtr_pag>1){ $MQ->data_seek($page-$reg_page); };
/* Pagination */
?>

<script>
edi_table('tabl_area',1);

$('#name_area').keyup(function() { 
	if ($(this).val().length > 1){
		GET_ajax('admi/tab_area','admi_area','form_area');
	} 
});
function delete_area(iden){
	if(confirm('Se eliminará este elemento, esta acción no puede deshacerse,\n¿Está seguro?')){
		$('#del_a').val(iden);
		GET_ajax('admi/tab_area','admi_area','form_area');
	}
}
</script>
<form method="GET" name="form_area" id="form_area" ><?php
hiddens(array('add_a'=>0,'del_a'=>0,'co_a'=>0,'order_by_a'=>$order_by_a,'asort_by'=>$asort_by)); ?>
<h2>AREA</h2>
<table id='tabl_area'>
	<tr>
		<th>
			<div onclick="$('#co_a').val(1);$('#order_by_a').val('name_area'); GET_ajax('admi/tab_area','admi_area','form_area');">Nombre <?php orderbtn('name_area'); ?> </div>
			<div>
				<input type="text" class="search_tbl" id="name_area" name="name_area" placeholder="Nombre" onchange="GET_ajax('admi/tab_area','admi_area','form_area');" value="<?php echo $name_area; ?>">
				<input type="button" name="brn_add" value="Agregar" style="<?php if(!$name_area)echo "display:none" ?>" onclick="$('#add_a').val(1);GET_ajax('admi/tab_area','admi_area','form_area');">
			</div>
		</th>
	</tr><?php
	$cont=$page-$reg_page;
	while($MFA=$MQ->fetch_array()){

		if($cont>=$page)break;
		echo "
		<tr>
			<td class='edi_table'>
				<div>$MFA[name_area]</div>
				<div class='selected'>".edi_table('stru','area','name_area',$MFA[iden_area],'text')."<span class='delete icomoon_ultimate' onclick='delete_area($MFA[iden_area]);'></span></div>
			</td>
		</tr>
		";
		
		$cont++;
	}?>
</table><?php
/* Pagination */
pagination($reg_num,"'admi/tab_area','admi_area','form_area'",$reg_page,'');
/* Pagination */
?>
</form>