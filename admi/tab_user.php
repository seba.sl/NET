<?php
if(!isset($_SESSION)) session_start();
if(!isset($cnx)) include("../inc/cnxi.php");
include('../inc/function.php');

$where="";
foreach($_GET as $k=>$v){
	$$k=$cnx->real_escape_string($v); 
	if(!in_array($k,array('frm_name','_','order_by','asort_by','filtr_pag','co'))){
		if(is_numeric($k))$where.=" AND $k='$v'";
		else $where.=" AND $k like '%$v%'";
	}
}

/* Order */
if($_GET[co]){
	if(!$asort_by || $asort_by=="ASC") $asort_by="DESC"; 
	elseif($asort_by=="DESC")$asort_by="ASC"; 
}

function orderbtn($tg){
	$t_class=($_GET[order_by]==$tg)?'active':'';
	if(!isset($_GET[order_by]) || $_GET[order_by]<>$tg) $t_icon='';
	else $t_icon=(($_GET[asort_by]=='DESC' && $_GET[order_by]==$tg)?'':'');
	
	echo "<span class=\"icomoon_ultimate $t_class\" > $t_icon </span>";
}

$order=($order_by and $asort_by)?"$order_by $asort_by":"name_user ASC";
/* Order */

$MQ=$cnx->query("SELECT stru__user.* FROM stru__user WHERE 1 $where ORDER BY $order");

/* Pagination */
$reg_num=$MQ->num_rows;
$reg_page=10;

$page=($filtr_pag*$reg_page);
if(!$page)$page=$reg_page;
if($filtr_pag>1){ $MQ->data_seek($page-$reg_page); };
/* Pagination */
?>

<script>
edi_table('tabl_user'); // <- id unica de la tabla a editar 
</script>

<form method="GET" name="form_user" id="form_user" ><?php
hiddens(array('co'=>0,'order_by'=>$order_by,'asort_by'=>$asort_by)); ?>
<h2>USUARIOS</h2>
<table id='tabl_user'>
	<tr>
		<th>
			<div onclick="$('#co').val(1);$('#order_by').val('name_user'); GET_ajax('admi/tab_user','admi_user','form_user');">Nombre
				<?php orderbtn('name_user'); ?>
			</div>
			<div><input type="text" class="search_tbl" name="name_user" placeholder="Nombre" onchange="GET_ajax('admi/tab_user','admi_user','form_user');" value="<?php echo $name_user; ?>"></div>
		</th>
		<th>
			<div onclick="$('#co').val(1);$('#order_by').val('mail_user'); GET_ajax('admi/tab_user','admi_user','form_user');">E-mail
				<?php orderbtn('mail_user'); ?>
			</div>
			<div><input type="text" class="search_tbl" name="mail_user" placeholder="E-mail" onchange="GET_ajax('admi/tab_user','admi_user','form_user');" value="<?php echo $mail_user; ?>"></div>
		</th>
		<th>
			<div onclick="$('#co').val(1);$('#order_by').val('role_user'); GET_ajax('admi/tab_user','admi_user','form_user');">Rol
				<?php orderbtn('role_user'); ?>
			</div>
			<div><input type="text" class="search_tbl" name="role_user" placeholder="Rol" onchange="GET_ajax('admi/tab_user','admi_user','form_user');" value="<?php echo $role_user; ?>"></div>
		</th>
		<th>
			<div onclick="$('#co').val(1);$('#order_by').val('posi_user'); GET_ajax('admi/tab_user','admi_user','form_user');">Cargo
				<?php orderbtn('posi_user'); ?>
			</div>
			<div><input type="text" class="search_tbl" name="posi_user" placeholder="Cargo" onchange="GET_ajax('admi/tab_user','admi_user','form_user');" value="<?php echo $posi_user; ?>"></div>
		</th>
		<th>
			<div onclick="$('#co').val(1);$('#order_by').val('birt_user'); GET_ajax('admi/tab_user','admi_user','form_user');">Cumpleaños
				<?php orderbtn('birt_user'); ?>
			</div>
			<div><input type="date" class="search_tbl" name="birt_user" placeholder="Cumpleaños" onchange="GET_ajax('admi/tab_user','admi_user','form_user');" value="<?php echo $birt_user; ?>"></div>
		</th>
	</tr><?php
	$cont=$page-$reg_page;
	while($MFA=$MQ->fetch_array()){

		if($cont>=$page)break;
		if($MFA[birt_user])$MFA[birt_user]=date('d/m', strtotime($MFA[birt_user])); else $MFA[birt_user]="";
		
		$role=[0=>'Ban',1=>'Usuario',2=>'Mod',3=>'Admin'];
		echo "
		<tr>
			<td>$MFA[name_user]</td>
			<td>$MFA[mail_user]</td>
			<td class='edi_table'>
				<div>".$role[$MFA[role_user]]."</div>
				<div>".edi_table('stru','user','role_user',$MFA[iden_user],$role)."</div>
			</td>
			<td class='edi_table'>
				<div>$MFA[posi_user]</div>
				<div>".edi_table('stru','user','posi_user',$MFA[iden_user],'text')."</div>
			</td>
			<td class='edi_table'>
				<div>$MFA[birt_user]</div>
				<div>".edi_table('stru','user','birt_user',$MFA[iden_user],'date')."</div>
			</td>
		</tr>
		";
		
		$cont++;
	}?>
</table><?php
/* Pagination */
pagination($reg_num,"'admi/tab_user','admi_user','form_user'",$reg_page,'');
/* Pagination */
?>
</form>